import utils

dic = {}
dicIp = {}
dicFakeUser = {}


with open('auth.V2.log') as file:
    for line in file:
        fakeUser = ip = user = line.rstrip().split(' ')
        user = user[8]
        ipIndex = 0

        if user == 'invalid':
            fakeUser = fakeUser[10]
            utils.incrementDic(fakeUser, dicFakeUser)
            ipIndex = 12
        else:
            utils.incrementDic(user, dic)
            ipIndex = 10

        utils.incrementDic(ip[ipIndex], dicIp)

utils.print_result(dicFakeUser, 'Les 10 logins faux existant les plus essayés en SSH sont :')
print('--------------------------------------------------')
utils.print_result(dic, 'Les 10 logins existant les plus essayés en SSH sont :')
print('--------------------------------------------------')
utils.print_result(dicIp, 'Les 10 adresses IPV4 les plus utilisées sont :')
