import utils

dic = {}
with open('auth.V1.log') as file:
    for line in file:
        user = line.rstrip().split(' ')[10]
        utils.incrementDic(user, dic)

utils.print_result(dic, 'Les 10 logins faux les plus essayés en SSH sont :')
