import urllib.request
import json


def requestGeolocation(ip: str):
    apiKey = 'd802faa0-10bd-11ec-b2fe-47a0872c6708'
    with urllib.request.urlopen("https://geolocation-db.com/json/{}/{}".format(apiKey, ip)) as url:
        return json.loads(url.read().decode())


def print_result(dictionary, message, contryRequest=False, maxIt=10):
    print(message)
    i = 0
    for k, v in sorted(dictionary.items(), key=lambda item: item[1], reverse=True):
        i += 1
        if contryRequest:
            data = requestGeolocation(k)
        print('#{} : {} {}({} fois)'.format(i, k, 'from {} '.format(data['country_name']) if contryRequest else '', v))
        if maxIt != 0:
            if i == maxIt:
                break


def incrementDic(key, dictionary):
    if key not in dictionary:
        dictionary[key] = 0

    dictionary[key] = dictionary[key] + 1
