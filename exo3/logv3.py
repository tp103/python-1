import utils


logs = []
dicIp = {}


def exo3(callback):
    with open('auth.V3.log') as file:
        for line in file:
            lineSplit = line.rstrip().split(' ')
            if lineSplit[5] in ['Failed', 'Invalid']:
                if lineSplit[8] == 'invalid':
                    utils.incrementDic(lineSplit[12], dicIp)
                elif lineSplit[9] == 'from':
                    utils.incrementDic(lineSplit[10], dicIp)
                # logs.append(line)
        callback(dicIp)


def callback(dictionary):
    utils.print_result(dictionary, 'Les 10 adresses IPV4 les plus utilisées sont :', True, 0)


exo3(callback)
