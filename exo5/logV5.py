import json

import utils

highCartContent = "Highcharts.chart('%s',{chart:{type:'pie'},title:{text:'%s'}," \
            "subtitle:{text:''},series:[{data:%s}]});"

dic = {}
dicIp = {}
dicFakeUser = {}

with open('./auth.big.log') as lines:
    for line in lines:
        lineSplit = line.rstrip().split(' ')
        if lineSplit[5] not in ['Failed', 'Invalid']:
            continue

        if lineSplit[5] == 'Failed':
            utils.incrementDic(lineSplit[12], dicIp)
            if lineSplit[8] == 'invalid':
                utils.incrementDic(lineSplit[10], dicFakeUser)
            else:
                utils.incrementDic(lineSplit[8], dic)
        elif lineSplit[5] == 'Invalid':
            utils.incrementDic(lineSplit[9], dicIp)
        elif lineSplit[5] == 'Disconnected':
            utils.incrementDic(lineSplit[7], dicIp)
        elif lineSplit[5] == 'Received':
            utils.incrementDic(lineSplit[8], dicIp)

# def highCart(dic, file, title):
#     docList = []
#     i = 0
#     total = 0
#     for k, v in sorted(dic.items(), key=lambda item: item[1], reverse=True):
#         i += 1
#         if i <= 14:
#             docList.append([k, v])
#         else:
#             total += v
#
#     docList.append(['Autre', total])
#     with open('{}.js'.format(file), 'w') as ojkggggg:
#         ojkggggg.write(highCartContent % (file, title, docList))
#         ojkggggg.close()
#
#
# highCart(dicFakeUser, 'serie1', 'Users inconnu utilisee pour des attaques SSH')
# highCart(dic, 'serie2', 'Users connu utilisee pour des attaques SSH')

print(len(dicIp))
list = []
for k, v in sorted(dicIp.items(), key=lambda item: item[1], reverse=True):
    data = utils.requestGeolocation(k)
    if data['latitude'] != 'Not found' and data['longitude'] != 'Not found':
        list.append(["{} ({} attaques)".format(k, v), data['latitude'], data['longitude']])

with open('localisations2.js', 'w') as localisation:
    localisation.write("const locations = {}".format(json.dumps(list)))
    localisation.close()
