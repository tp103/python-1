import json

import utils
from exo3 import logv3


def callback(dictionary):
    list = []
    for k, v in sorted(dictionary.items(), key=lambda item: item[1], reverse=True):
        data = utils.requestGeolocation(k)
        if data['latitude'] != 'Not found' and data['longitude'] != 'Not found':
            list.append(["{} ({} attaques)".format(k, v), data['latitude'], data['longitude']])

    with open('localisations.js', 'w') as localisation:
        localisation.write("const locations = {}".format(json.dumps(list)))
        localisation.close()


logv3.exo3(callback)
